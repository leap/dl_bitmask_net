Bitmask downloads
-----------------------------------------

This is the Bitmask download website (https://dl.bitmask.net)

It is entirely static, but relies on a bunch of apache tricks for things like language negotiation and pretty directory indexes.

The static content files in ./public/ are rendered from the content in ./pages/ using the command `amber rebuild`.

To install amber:

    gem install amber

See https://github.com/elijh/amber for more information.

Requirements
-----------------------------------------

The following apache mods are required:

    sudo a2enmod include rewrite autoindex mime negotiation

The following apache directives are required, on the document root:

    Options +Indexes
    IndexOptions +FancyIndexing +Charset=UTF-8
    IndexOptions +SuppressRules +SuppressDescription +NameWidth=*
    IndexOptions +FoldersFirst +VersionSort
    IndexOptions +SuppressHTMLPreamble +HTMLTable
    HeaderName /assets/header.html
    ReadmeName /assets/footer.html

    AddIcon /assets/icons/blank.png ^^BLANKICON^^
    AddIcon /assets/icons/folder.png ^^DIRECTORY^^
    AddIcon /assets/icons/folder-home.png ..
    AddIcon /assets/icons/audio.png .aif .iff .m3u .m4a .mid .mp3 .mpa .ra .wav .wma .f4a .f4b .oga .ogg
    AddIcon /assets/icons/bin.png .bin
    AddIcon /assets/icons/c.png .c
    AddIcon /assets/icons/calc.png .xlsx .xlsm .xltx .xltm .xlam .xlr .xls .csv
    AddIcon /assets/icons/css.png .css
    AddIcon /assets/icons/deb.png .deb
    AddIcon /assets/icons/doc.png .doc .docx .docm .dot .dotx .dotm .log .msg .odt .pages .rtf .tex .wpd .wps
    AddIcon /assets/icons/draw.png .svg
    AddIcon /assets/icons/eps.png .ai .eps
    AddIcon /assets/icons/gif.png .gif
    AddIcon /assets/icons/html.png .html .xhtml .shtml .htm
    AddIcon /assets/icons/ico.png .ico
    AddIcon /assets/icons/java.png .jar
    AddIcon /assets/icons/jpg.png .jpg .jpeg
    AddIcon /assets/icons/js.png .js .json
    AddIcon /assets/icons/markdown.png .md
    AddIcon /assets/icons/package.png .pkg .dmg
    AddIcon /assets/icons/pdf.png .pdf
    AddIcon /assets/icons/php.png .php .phtml
    AddIcon /assets/icons/png.png .png
    AddIcon /assets/icons/ps.png .ps
    AddIcon /assets/icons/psd.png .psd
    AddIcon /assets/icons/rar.png .rar
    AddIcon /assets/icons/rb.png .rb
    AddIcon /assets/icons/rpm.png .rpm
    AddIcon /assets/icons/rss.png .rss
    AddIcon /assets/icons/sql.png .sql
    AddIcon /assets/icons/tiff.png .tiff
    AddIcon /assets/icons/text.png .txt .nfo
    AddIcon /assets/icons/video.png .asf .asx .avi .flv .mkv .mov .mp4 .mpg .rm .srt .swf .vob .wmv .m4v .f4v .f4p .ogv
    AddIcon /assets/icons/xml.png .xml
    AddIcon /assets/icons/zip.png .zip
    DefaultIcon /assets/icons/default.png

Credits
-----------------------------------------

Dir listing style from Apaxy - https://github.com/AdamWhitcroft/Apaxy
License: in the public domain.

Icons are from "Faenza Icons" -- http://tiheum.deviantart.com/art/Faenza-Icons-173323228
License: GNU General Public License.

Stylesheet is from Bootstrap -- http://getbootstrap.com
License: Apache, version 2 -- https://www.apache.org/licenses/LICENSE-2.0.html
  customized with:
    common css: typography, code, grid, tables, buttons
    components: breadcrumbs, badges, jumbotron, thumbnails, alerts, list groups, wells
    utilities: basic, responsive

The fonts are Font Awesome -- http://fortawesome.github.io
License: SIL OFL 1.1 -- http://scripts.sil.org/OFL
