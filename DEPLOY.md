The fully rendered pages must be committed to this repo for them to show up.
This means you need to make sure to run a 'amber rebuild' before you commit
any change.

To deploy dl_bitmask_net to dl.bitmask.net:

    git clone ssh://gitolite@leap.se/dl_bitmask_net
    cd dl_bitmask_net
    amber rebuild
    git commit ...
    git push

The post commit hooks should update dl.bitmask.net automatically, but if they
do not:

    ssh loris.leap.se -l root
    cd /var/www/dl_bitmask_net
    su www-data
    git checkout master
    git pull
