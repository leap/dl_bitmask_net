## Configuring email

To get encrypted email to work, you have two options:

1. [Bitmask Thunderbird Extension](https://addons.mozilla.org/en-us/thunderbird/addon/bitmask/) (recommended)
2. Manual email client configuration

#### Bitmask Thunderbird Extension

1. Install Bitmask app and register for an account.
2. Install the [Bitmask Thunderbird Extension](https://addons.mozilla.org/en-us/thunderbird/addon/bitmask/).
3. Select the Thunderbird menu item `File` > `New` > `Bitmask Account...`

#### Manual email client configuration

* IMAP -- `localhost:1984`
  * username: the full email address you use with the Bitmask app.
  * password: ignored, can be anything.
  * SSL/TLS: off
* SMTP -- `localhost:2013`
  * authentication: none
  * SSL/TLS: off

You should also disable any local caching in your email client, if applicable. All the data stored by Bitmask is kept on disk in an encrypted format, but your mail client might cache your email in clear text, so it is good to disable caching if you can. Also, all your Bitmask email is sync'ed locally anyway, so there is no benefit to local caching.
